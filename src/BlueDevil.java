import java.util.Scanner;

public class BlueDevil extends Person {
	String major;
	String role;
	private BlueDevil[] persons;
	public BlueDevil() {
		
	}
	public BlueDevil(String name_,String country_, String major_, String role_) {
		name = name_;
		country = country_;
		major = major_;
		role = role_;
	}
	void whoIs (String name_to_find,BlueDevil[] persons,int count) {
		boolean flag = false;
		for (int i =0;i<count;i++) {
		if (persons[i].name.equals(name_to_find) ) {
			flag = true;
			System.out.println(persons[i].name+" is from "+persons[i].country+ ". He/She is the "
				+persons[i].role+ " from the "+	persons[i].major+ " department.");
		}
		}
		if (flag == false)
		System.out.println(name_to_find+" not found");
	}
	
	void where (String place_to_find,BlueDevil[] persons,int count) {
		boolean flag = false;
		for (int i =0;i<count;i++) {
			System.out.println("i is "+i);
		if (persons[i].country.equals(place_to_find) ) {
			flag = true;
			System.out.println(persons[i].name+" is from "+persons[i].country);
		}
		}
		if (flag == false) {
		System.out.println("No one is from "+ place_to_find);
		}
	
	}
	
	void job (String job,BlueDevil[] persons,int count) {
		boolean flag = false;
		for (int i =0;i<count;i++) {
		if (persons[i].role.equals(job) )
		{
			flag = true;
			System.out.println(persons[i].name+" is "+persons[i].role);
		}
		}
		if (flag == false)
		System.out.println("No one is " + job);
	}
	
	void department (String department_to_find,BlueDevil[] persons,int count) {
		boolean flag = false;
		for (int i =0;i<count;i++) {
		if (persons[i].role.equals(department_to_find) ) {
			flag = true;
			System.out.println(persons[i].name+" is from "+ persons[i].major+ " department");
		}
		}
		if (flag == false)
		System.out.println("No one is from "+ department_to_find + " department");
	}
	public static void main (String[] args) {
		BlueDevil[] persons = new BlueDevil[100];
		//Yuqiao Liang
		persons[0] = new BlueDevil("Yuqiao Liang","China","ECE", "student");
		//Adel Fahmy
		persons[1] = new BlueDevil("Adel Fahmy","US","ECE", "Adjunct Assistant Professor");
		//Ric Telford
		persons[2] = new BlueDevil("Ric Telford","US","ECE", "Adjunct Assistant Professor");
		//Yuanyuan Yu
		persons[3] = new BlueDevil("Yuanyuan Yu","China","ECE", "TA");
		//You Lyu
		persons[4] = new BlueDevil("You Lyu","China","ECE", "TA");
		int count = 5;
		BlueDevil hw2 = new BlueDevil();
		Scanner scan = new Scanner(System.in);
		while(true) {
		System.out.println("Please enter the mode.(add / search / END)");
		String mode = scan.nextLine();
		if (mode.equals("search")) {		
		System.out.println("Please enter how you want to search? (name, country, role, department),");
		String field = scan.nextLine();
		if (field.equals ("name")) { // find by name
		System.out.println("Please enter the name you are looking for:");
		String name = scan.nextLine();
		//System.out.println(name+"over");
		hw2.whoIs(name,persons,count);
		}
		else if (field.equals ("country")) { // find by country
			System.out.println("Please enter the country you are looking for:");
			String name = scan.nextLine();
			//System.out.println(name+"over");
			hw2.where(name,persons,count);
		}
		else if (field.equals ("role")) { // find by role
			System.out.println("Please enter the role you are looking for:");
			String name = scan.nextLine();
			//System.out.println(name+"over");
			hw2.job(name,persons,count);
		}
		else if (field.equals ("department")) { // find by department
			System.out.println("Please enter the department you are looking for:");
			String name = scan.nextLine();
			//System.out.println(name+"over");
			hw2.department(name,persons,count);
		}
		else {
			System.out.println("Please enter the valid field (name / country / role / department)");
		}
		}
		else if (mode.equals("add")) {
			if (count <=99) {
			System.out.println("Please enter the name of new person:");
			String newname = scan.nextLine();
			System.out.println("Please enter the country of new person:");
			String newcountry = scan.nextLine();
			System.out.println("Please enter the role of new person:");
			String newrole = scan.nextLine();
			System.out.println("Please enter the department of new person:");
			String newdepartment = scan.nextLine();
			persons[count] = new BlueDevil(newname,newcountry,newdepartment,newrole);
			count++;
			}
			else {
				System.out.println("exceed the max amount");
			}
		}
		else if (mode.equals("END")) {
			System.out.println("Thank you. Have a nice day! :D");
			break;
		}
		else {
			System.out.println("Please enter valid command. (add / search / END)");
		}	
	}
		return;
	}
}
